/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Adapter used in conjunction with {@link SeamDrawablePanel} to initialize the former with a proper image file.
 * 
 * @author Bartlomiej Pietrzyk Date: 16 sie 2013
 */
public class SeamInitializeAdapter extends MouseAdapter {

    private final SeamDrawablePanel panel;

    public SeamInitializeAdapter(final SeamDrawablePanel panel) {
        this.panel = panel;
    }

    @Override
    public void mouseClicked(final MouseEvent arg0) {
        final JFileChooser jFileDialog = new JFileChooser(new File(new File(new File("d:"), "tmp"), "seam_carving"));
        if (JFileChooser.APPROVE_OPTION == jFileDialog.showOpenDialog(panel)) {
            try {
                final BufferedImage bufferedImage = ImageIO.read(jFileDialog.getSelectedFile());
                if (bufferedImage != null) {
                    final SeamCarver carver = new SeamCarver(bufferedImage);
                    panel.setSeamCarver(carver);
                    panel.repaint();
                }
            } catch (final IOException e) {
                JOptionPane.showMessageDialog(panel,
                    String.format("Could not read file: %s" + jFileDialog.getSelectedFile()));
            }
        }
    }
}
