/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.image.BufferedImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Bartlomiej Pietrzyk Date: 8 wrz 2013
 */
public class GradientEnergyCalculator {

    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GradientEnergyCalculator.class);

    public static double calculateEnergy(final int x, final int y, final BufferedImage image) {
        if (onBorder(x, y, image)) {
            return 255 * 255 * 3;
        } else {
            return gradSquare(x, y, image);
        }
    }

    private static boolean onBorder(final int x, final int y, final BufferedImage image) {
        return x == 0 || x == image.getWidth() - 1 || y == 0 || y == image.getHeight() - 1;
    }

    private static int gradSquare(final int x, final int y, final BufferedImage image) {
        try {
            final int left = image.getRGB(x - 1, y);
            final int right = image.getRGB(x + 1, y);
            final int top = image.getRGB(x, y - 1);
            final int bottom = image.getRGB(x, y + 1);
            final int dRedX = (right >> 16 & 0xFF) - (left >> 16 & 0xFF);
            final int dRedY = (top >> 16 & 0xFF) - (bottom >> 16 & 0xFF);
            final int dGreenX = (right >> 8 & 0xFF) - (left >> 8 & 0xFF);
            final int dGreenY = (top >> 8 & 0xFF) - (bottom >> 8 & 0xFF);
            final int dBlueX = (right >> 0 & 0xFF) - (left >> 0 & 0xFF);
            final int dBlueY = (top >> 0 & 0xFF) - (bottom >> 0 & 0xFF);
            final int gradX = dRedX * dRedX + dGreenX * dGreenX + dBlueX * dBlueX;
            final int gradY = dRedY * dRedY + dGreenY * dGreenY + dBlueY * dBlueY;
            return gradX + gradY;
        } catch (final ArrayIndexOutOfBoundsException e) {
            LOGGER.error("x={}, y={}", x, y);
            throw e;
        }
    }
}
