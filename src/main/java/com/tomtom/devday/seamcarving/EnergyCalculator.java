/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.image.BufferedImage;

/**
 * Component calculates the picture energy function.
 * 
 * @author Bartlomiej Pietrzyk Date: 23 sie 2013
 */
public interface EnergyCalculator {

    double[] calculateEnergy(BufferedImage image);
}
