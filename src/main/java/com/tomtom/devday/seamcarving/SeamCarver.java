/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.Raster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Bartlomiej Pietrzyk Date: 13 sie 2013
 */
public class SeamCarver {

    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SeamCarver.class);

    private BufferedImage image;
    private double[] energy;

    public SeamCarver(final BufferedImage picture) {
        image = picture;
        energy = new ThreadedColorGradientEnergyCalculator().calculateEnergy(image);
    }

    private int[] findSeam(final int width, final int height, final boolean isReversed) {
        int indOfMin = 0;
        double[] distTo = new double[width];
        final int[][] edgeTo = new int[height][width];
        final double[] tempDistTo = new double[width];
        for (int row = 1; row < height; ++row) { // first (0) row has 0 distanceToEnd so we start from second(1) row.
            edgeTo[row][0] = 1;
            tempDistTo[0] = getEnergy(0, row, isReversed) + distTo[1];
            edgeTo[row][width - 1] = width - 2;
            tempDistTo[width - 1] = getEnergy(width - 1, row, isReversed) + distTo[width - 2];
            for (int col = 1; col < width - 1; col++) {
                indOfMin = findIndexOfMinimumEnergyStep(distTo, col);
                edgeTo[row][col] = indOfMin;
                tempDistTo[col] = getEnergy(col, row, isReversed) + distTo[indOfMin];
            }
            distTo = tempDistTo.clone();
        }
        final int[] seam = new int[height];
        seam[height - 1] = findIndexOfGlobalMin(distTo);
        for (int row = height - 2; row > -1; row--) {
            seam[row] = edgeTo[row + 1][seam[row + 1]];
        }
        return seam;
    }

    private int findIndexOfMinimumEnergyStep(final double[] distTo, final int col) {
        int indOfMin = col + 1;
        if (distTo[col] < distTo[indOfMin]) {
            indOfMin = col;
        }
        if (distTo[col - 1] < distTo[indOfMin]) {
            indOfMin = col - 1;
        }

        return indOfMin;
    }

    private int findIndexOfGlobalMin(final double[] arr) {
        final int last = arr.length - 1;
        int indOfMin = last;
        double min = arr[last];
        for (int index = last; index > -1; index--) {
            if (arr[index] < min) {
                indOfMin = index;
                min = arr[index];
            }
        }
        return indOfMin;
    }

    private double getEnergy(final int x, final int y, final boolean isReversed) {
        return isReversed ? energy[y + x * image.getWidth()] : energy[x + y * image.getWidth()];
    }

    public BufferedImage getImage() {
        return image;
    }

    public int getWidth() {
        return image.getWidth();
    }

    public int getHeight() {
        return image.getHeight();
    }

    public int[] findHorizontalSeam() {
        return findSeam(getHeight(), getWidth(), true);
    }

    public int[] findVerticalSeam() {
        return findSeam(getWidth(), getHeight(), false);
    }

    public void removeHorizontalSeam(final int[] seam) {
        if (seamInvalid(seam, getWidth())) {
            throw new java.lang.IllegalArgumentException();
        }
        final int pixelStride = 3;
        final int maxRowIdx = findMax(seam);
        final int decHeight = getHeight() - 1, width = getWidth();
        final byte[] dstBuffer = new byte[decHeight * image.getWidth() * pixelStride];
        final byte[] srcBuffer = ((DataBufferByte)image.getData().getDataBuffer()).getData();

        int dstIdx = 0, srcIdx = 0, seamIdx = 0, idx = 0;
        try {

            if (maxRowIdx > 0) {
                System.arraycopy(srcBuffer, 0, dstBuffer, 0, maxRowIdx * width * pixelStride);
            }
            for (idx = 0; idx < seam.length; ++idx) {
                seamIdx = seam[idx];
                while (seamIdx < maxRowIdx) {
                    dstIdx = (seamIdx * width + idx) * pixelStride;
                    srcIdx = ((seamIdx + 1) * width + idx) * pixelStride;
                    dstBuffer[dstIdx] = srcBuffer[srcIdx];
                    dstBuffer[dstIdx + 1] = srcBuffer[srcIdx + 1];
                    dstBuffer[dstIdx + 2] = srcBuffer[srcIdx + 2];
                    ++seamIdx;
                }
            }
            if (maxRowIdx + 1 < getHeight()) {
                System.arraycopy(//
                    srcBuffer, (maxRowIdx + 1) * width * pixelStride, //
                    dstBuffer, maxRowIdx * width * pixelStride, //
                    srcBuffer.length - (maxRowIdx + 1) * width * pixelStride);
            }
            image =
                new BufferedImage(//
                    image.getColorModel(), //
                    Raster.createWritableRaster(new PixelInterleavedSampleModel(DataBuffer.TYPE_BYTE, image.getWidth(),
                        decHeight, //
                        pixelStride, image.getWidth() * pixelStride, new int[] {2, 1, 0}), //
                        new DataBufferByte(dstBuffer, dstBuffer.length, 0), null), //
                    false, null);
            updateEnergyAfterHorizontalSeamRemoval(seam);
        } catch (final ArrayIndexOutOfBoundsException e) {
            LOGGER.error(
                "Horizontal Seam srcLen={}, srcIdx={}, dstLen={}, dstIdx={}, seamIdx={}, maxRowIdx={}, width={}, height={}",
                srcBuffer.length, srcIdx, dstBuffer.length, dstIdx, seamIdx, maxRowIdx, getWidth(), getHeight());
            throw e;
        }
    }

    /**
     * @param seam
     */
    private void updateEnergyAfterHorizontalSeamRemoval(final int[] seam) {
        final int pixelStride = 1;
        final int maxRowIdx = findMax(seam);
        final int height = getHeight(), width = getWidth();
        final double[] dstBuffer = new double[height * image.getWidth() * pixelStride];
        final double[] srcBuffer = energy;

        int dstIdx = 0, srcIdx = 0, idx = 0, sIdx = 0;
        try {
            if (maxRowIdx > 0) {
                System.arraycopy(srcBuffer, 0, dstBuffer, 0, maxRowIdx * width * pixelStride);
            }
            for (idx = 0; idx < seam.length; ++idx) {
                sIdx = seam[idx];
                while (sIdx < maxRowIdx) {
                    dstIdx = (sIdx * width + idx) * pixelStride;
                    srcIdx = ((sIdx + 1) * width + idx) * pixelStride;
                    dstBuffer[dstIdx] = srcBuffer[srcIdx];
                    ++sIdx;
                }
            }
            if (maxRowIdx + 1 < getHeight()) {
                System.arraycopy(//
                    srcBuffer, (maxRowIdx + 1) * width * pixelStride, //
                    dstBuffer, maxRowIdx * width * pixelStride, //
                    srcBuffer.length - (maxRowIdx + 1) * width * pixelStride);
            }

            for (idx = 0; idx < seam.length; ++idx) {
                sIdx = seam[idx];
                if (sIdx < height) {
                    dstBuffer[(sIdx * width + idx) * pixelStride] =
                        GradientEnergyCalculator.calculateEnergy(idx, sIdx, image);
                }
                if (sIdx > 0) {
                    dstBuffer[((sIdx - 1) * width + idx) * pixelStride] =
                        GradientEnergyCalculator.calculateEnergy(idx, sIdx - 1, image);
                }
            }

            energy = dstBuffer;
        } catch (final ArrayIndexOutOfBoundsException e) {
            LOGGER.error("Horizontal Energy srcLen={}, srcIdx={}, dstLen={}, dstIdx={}", srcBuffer.length, srcIdx,
                dstBuffer.length, dstIdx);
            throw e;

        }
    }

    /**
     * @param seam
     * @return
     */
    private int findMax(final int[] seam) {
        int max = Integer.MIN_VALUE;
        for (final int val : seam) {
            if (val > max) {
                max = val;
            }
        }
        return max;
    }

    public void removeVerticalSeam(final int[] seam) {
        if (seamInvalid(seam, getHeight())) {
            throw new java.lang.IllegalArgumentException();
        }
        final int pixelStride = 3;
        final int decWidth = getWidth() - 1;
        final byte[] dstBuffer = new byte[decWidth * image.getHeight() * pixelStride];
        final byte[] srcBuffer = ((DataBufferByte)image.getData().getDataBuffer()).getData();

        int srcIdx = 0, dstIdx = 0, idx = 0, length = seam[idx] * pixelStride;
        try {
            System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, length);
            srcIdx += length + pixelStride;
            dstIdx += length;
            ++idx;
            do {
                length = (seam[idx] + decWidth - seam[idx - 1]) * pixelStride;
                System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, length);
                srcIdx += length + pixelStride;
                dstIdx += length;
                ++idx;
            } while (idx < seam.length);
            System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, srcBuffer.length - srcIdx);

            image =
                new BufferedImage(//
                    image.getColorModel(), //
                    Raster.createWritableRaster(
                        new PixelInterleavedSampleModel(DataBuffer.TYPE_BYTE, decWidth, image.getHeight(), //
                            pixelStride, decWidth * pixelStride, new int[] {2, 1, 0}), //
                        new DataBufferByte(dstBuffer, dstBuffer.length, 0), null), //
                    false, null);
            updateEnergyAfterVerticalSeamRemoval(seam);
        } catch (final ArrayIndexOutOfBoundsException e) {
            LOGGER.error("Vertical Seam srcLen={}, srcIdx={}, dstLen={}, dstIdx={}, length={}", srcBuffer.length, srcIdx,
                dstBuffer.length, dstIdx, length);
            throw e;
        }
    }

    /**
     * @param seam
     */
    private void updateEnergyAfterVerticalSeamRemoval(final int[] seam) {
        final int pixelStride = 1;
        final int width = getWidth();
        final double[] srcBuffer = energy;
        final double[] dstBuffer = new double[width * getHeight() * pixelStride];

        int srcIdx = 0, dstIdx = 0, idx = 0, length = seam[idx] * pixelStride;
        try {
            System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, length);
            srcIdx += length + pixelStride;
            dstIdx += length;
            ++idx;
            do {
                length = (seam[idx] + width - seam[idx - 1]) * pixelStride;
                System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, length);
                srcIdx += length + pixelStride;
                dstIdx += length;
                ++idx;
            } while (idx < seam.length);
            length = srcBuffer.length - srcIdx;
            if (length != 0) {
                System.arraycopy(srcBuffer, srcIdx, dstBuffer, dstIdx, length);
            } else {
                LOGGER.info("Break on 0!");
            }

            for (int sIdx = 0; sIdx < seam.length - 1; ++sIdx) {
                length = seam[sIdx] + sIdx * width;
                dstBuffer[length - 1] = GradientEnergyCalculator.calculateEnergy(seam[sIdx] - 1, sIdx, image);
                dstBuffer[length] = GradientEnergyCalculator.calculateEnergy(seam[sIdx], sIdx, image);
            }

            final int sIdx = seam.length - 1;
            length = seam[sIdx] + sIdx * width;
            dstBuffer[length - 1] = GradientEnergyCalculator.calculateEnergy(seam[sIdx] - 1, sIdx, image);
            if (sIdx == width) {
                dstBuffer[length] = GradientEnergyCalculator.calculateEnergy(seam[sIdx], sIdx, image);
            }

            energy = dstBuffer;
        } catch (final ArrayIndexOutOfBoundsException e) {
            LOGGER.error("Vertical Energy srcLen={}, srcIdx={}, dstLen={}, dstIdx={}, length={}", srcBuffer.length, srcIdx,
                dstBuffer.length, dstIdx, length);
            throw e;
        }
    }

    private boolean seamInvalid(final int[] seam, final int length) {
        if (length < 2) {
            return true;
        }
        if (seam.length != length) {
            return true;
        }
        for (int ii = 1; ii < seam.length; ii++) {
            if (Math.abs(seam[ii] - seam[ii - 1]) > 1) {
                return true;
            }
        }

        return false;
    }
}
