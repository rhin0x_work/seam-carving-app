/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * @author Bartlomiej Pietrzyk Date: 16 sie 2013
 */
public class SeamDrawablePanel extends JPanel {

    private SeamCarver seamCarver;

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        if (seamCarver != null) {
            g.drawImage(seamCarver.getImage(), 0, 0, null);
        }
    }

    public SeamCarver getSeamCarver() {
        return seamCarver;
    }

    public void setSeamCarver(final SeamCarver seamCarver) {
        this.seamCarver = seamCarver;
        setSize(seamCarver.getWidth(), seamCarver.getHeight());
    }

}
