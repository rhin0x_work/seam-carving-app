/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Bartlomiej Pietrzyk Date: 13 sie 2013
 */
public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MainFrame.class);

    private final JPanel contentPane;

    /**
     * Launch the application.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                try {
                    final MainFrame frame = new MainFrame();
                    frame.setVisible(true);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MainFrame() {
        setTitle("SeamCarver");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        final SeamDrawablePanel panel = new SeamDrawablePanel();
        panel.addComponentListener(new SeamResizeAdapter(panel));
        panel.addMouseListener(new SeamInitializeAdapter(panel));
        contentPane.add(panel, BorderLayout.CENTER);
    }
}
