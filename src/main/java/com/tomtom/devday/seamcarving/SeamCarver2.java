package com.tomtom.devday.seamcarving;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class SeamCarver2 {

    private static final double BORDER_PIXEL_ENERGY = 195075.0d;
    private static final boolean HORIZONTAL = false;
    private static final boolean VERTICAL = true;

    private double[][] energy;
    private boolean validEnergy = false;

    private BufferedImage picture;

    public SeamCarver2(final BufferedImage picture) {
        this.picture = picture;
    }

    public BufferedImage getImage() {
        return picture;
    }

    public int getWidth() {
        return picture.getWidth();
    }

    public int getHeight() {
        return picture.getHeight();
    }

    public double energy(final int x, final int y) {
        validateSize(x, y);
        if (x == 0 || x == getWidth() - 1 || y == 0 || y == getHeight() - 1) {
            return BORDER_PIXEL_ENERGY;
        } else {
            final Color left = new Color(picture.getRGB(x - 1, y));
            final Color right = new Color(picture.getRGB(x + 1, y));
            final Color upper = new Color(picture.getRGB(x, y - 1));
            final Color down = new Color(picture.getRGB(x, y + 1));
            return Math.pow(right.getRed() - left.getRed(), 2) + Math.pow(right.getGreen() - left.getGreen(), 2)
                + Math.pow(right.getBlue() - left.getBlue(), 2) + Math.pow(down.getRed() - upper.getRed(), 2)
                + Math.pow(down.getGreen() - upper.getGreen(), 2) + Math.pow(down.getBlue() - upper.getBlue(), 2);
        }
    }

    private void validateSize(final int x, final int y) {
        if (x < 0 || x >= getWidth()) {
            throw new java.lang.IndexOutOfBoundsException("X:" + x);
        }
        if (y < 0 || y >= getHeight()) {
            throw new java.lang.IndexOutOfBoundsException("Y:" + y);
        }
    }

    private int[] findSeam(final boolean isVertical) {
        updateEnergy();
        final int seamWidth = getSeamWidth(isVertical);
        final int seamHeight = getSeamHeight(isVertical);
        final int[] seam = new int[seamHeight];
        double distTo[] = new double[seamWidth];
        final int edgeTo[][] = new int[seamHeight][seamWidth];
        for (int heightIdx = 1; heightIdx < seamHeight; ++heightIdx) {
            distTo = findMinEnery(isVertical, distTo, edgeTo, heightIdx);
        }

        int seamScanIndex = seamHeight - 1;
        seam[seamScanIndex] = findMinimum(edgeTo[seamScanIndex]);
        while (--seamScanIndex > 0) {
            seam[seamScanIndex] = edgeTo[seamScanIndex][seam[seamScanIndex + 1]];
        }
        return seam;
    }

    private int findMinimum(final int[] ds) {
        int min = Integer.MAX_VALUE, index = 0;
        for (int idx = 0; idx < ds.length; ++idx) {
            if (ds[idx] < min) {
                min = ds[idx];
                index = idx;
            }
        }
        return index;
    }

    private double[] findMinEnery(final boolean isVertical, final double[] distTo, final int[][] edgeTo, final int heightIdx) {
        final double[] rowEnergy = getRowEnergy(isVertical, heightIdx);
        final int seamWidth = getSeamWidth(isVertical);

        edgeTo[heightIdx][0] = 1;
        distTo[0] += rowEnergy[1];
        edgeTo[heightIdx][seamWidth - 1] = seamWidth - 2;
        distTo[seamWidth - 1] += rowEnergy[seamWidth - 2];
        for (int widthIdx = 1; widthIdx < seamWidth - 2; ++widthIdx) {
            final int min = findMinEnergy(isVertical, widthIdx, rowEnergy);
            distTo[widthIdx] += rowEnergy[min];
            edgeTo[heightIdx][widthIdx] = min;
        }
        return distTo;
    }

    private void updateEnergy() {
        if (validEnergy) {
            return;
        } else {
            final int pwidth = picture.getWidth();
            final int pheight = picture.getHeight();
            energy = new double[pwidth][pheight];
            for (int iwidth = 0; iwidth < pwidth; ++iwidth) {
                for (int iheight = 0; iheight < pheight; ++iheight) {
                    energy[iwidth][iheight] = energy(iwidth, iheight);
                }
            }
            validEnergy = true;
        }
    }

    private double[] getRowEnergy(final boolean isVertical, final int heightIdx) {
        if (isVertical) {
            final double[] result = new double[getSeamWidth(isVertical)];
            for (int idx = 0; idx < getSeamWidth(isVertical); ++idx) {
                result[idx] = energy[idx][heightIdx];
            }
            return result;
        } else {
            return energy[heightIdx];
        }
    }

    private int findMinEnergy(final boolean isVertical, final int widthIdx, final double[] distTo) {
        final int leftIdx = widthIdx - 1;
        final int rightIdx = widthIdx + 1;
        final double a = distTo[leftIdx];
        final double b = distTo[widthIdx];
        final double c = distTo[rightIdx];
        if (a <= b) {
            if (a <= c) {
                return leftIdx;
            } else {
                return rightIdx;
            }
        } else {
            if (b <= c) {
                return widthIdx;
            } else {
                return rightIdx;
            }
        }
    }

    public void removeHorizontalSeam(final int[] seam) {
        final int width = picture.getWidth();
        final int height = picture.getHeight();
        final BufferedImage seamed = new BufferedImage(width, height - 1, picture.getType());
        for (int col = 0; col < width; ++col) {
            for (int row = 0; row < height; ++row) {
                if (row < seam[col]) {
                    seamed.setRGB(col, row, picture.getRGB(col, row));
                } else if (row > seam[col]) {
                    seamed.setRGB(col, row - 1, picture.getRGB(col, row));
                }
            }
        }
        picture = seamed;
        validEnergy = false;
    }

    public void removeVerticalSeam(final int[] seam) {
        final int width = picture.getWidth();
        final int height = picture.getHeight();
        final BufferedImage seamed = new BufferedImage(width - 1, height, picture.getType());
        for (int col = 0; col < width; ++col) {
            for (int row = 0; row < height; ++row) {
                if (col < seam[row]) {
                    seamed.setRGB(col, row, picture.getRGB(col, row));
                } else if (col > seam[row]) {
                    seamed.setRGB(col - 1, row, picture.getRGB(col, row));
                }
            }
        }
        picture = seamed;
        validEnergy = false;
    }

    private double getEnergy(final boolean isVertical, final int row, final int col) {
        if (isVertical) {
            return energy[row][col];
        } else {
            return energy[col][row];
        }
    }

    private int getSeamHeight(final boolean isVertical) {
        if (isVertical) {
            return energy[0].length;
        } else {
            return energy.length;
        }
    }

    private int getSeamWidth(final boolean isVertical) {
        if (isVertical) {
            return energy.length;
        } else {
            return energy[0].length;
        }
    }

    public int[] findVerticalSeam() {
        return findSeam(VERTICAL);
    }

    public int[] findHorizontalSeam() {
        return findSeam(HORIZONTAL);
    }

}
