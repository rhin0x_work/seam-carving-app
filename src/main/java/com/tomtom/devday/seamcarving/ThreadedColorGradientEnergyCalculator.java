/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Bartlomiej Pietrzyk Date: 23 sie 2013
 */
public class ThreadedColorGradientEnergyCalculator implements EnergyCalculator {

    private final ExecutorService executorService;
    private final int nrOfCores;

    public ThreadedColorGradientEnergyCalculator() {
        nrOfCores = Runtime.getRuntime().availableProcessors() * 2;
        executorService = Executors.newFixedThreadPool(nrOfCores);
    }

    @Override
    public double[] calculateEnergy(final BufferedImage image) {
        final int stripWidth = image.getWidth() / nrOfCores;
        final double[] energy = new double[image.getWidth() * image.getHeight()];
        final List<Future<?>> results = new ArrayList<Future<?>>(image.getWidth());

        int stripIdx = 0;
        while (stripIdx + stripWidth < image.getWidth()) {
            results.add(executorService.submit(new ProcessImageStrip(stripIdx, stripIdx + stripWidth, image, energy)));
            stripIdx += stripWidth;
        }
        results.add(executorService.submit(new ProcessImageStrip(stripIdx, image.getWidth(), image, energy)));

        try {
            for (final Future<?> future : results) {
                future.get();
            }
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }
        return energy;
    }

    private static class ProcessImageStrip implements Runnable {

        private final int stripStartIdx;
        private final int stripEndIdx;
        private final double[] energy;
        private final BufferedImage image;

        public ProcessImageStrip(final int stripStartIdx, final int stripEndIdx, final BufferedImage image,
            final double[] energy) {
            this.image = image;
            this.energy = energy;
            this.stripStartIdx = stripStartIdx;
            this.stripEndIdx = stripEndIdx;
        }

        @Override
        public void run() {
            for (int x = stripStartIdx; x < stripEndIdx; ++x) {
                for (int y = 0; y < image.getHeight(); ++y) {
                    energy[x + y * image.getWidth()] = GradientEnergyCalculator.calculateEnergy(x, y, image);
                }
            }
        }
    }

}
