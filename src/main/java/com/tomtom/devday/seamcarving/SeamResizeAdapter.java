/**
 * 
 */
package com.tomtom.devday.seamcarving;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Bartlomiej Pietrzyk Date: 16 sie 2013
 */
public class SeamResizeAdapter extends ComponentAdapter {

    /** Logger for this class. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SeamResizeAdapter.class);

    private final SeamDrawablePanel panel;

    public SeamResizeAdapter(final SeamDrawablePanel panel) {
        this.panel = panel;
    }

    @Override
    public void componentResized(final ComponentEvent arg0) {
        final Dimension size = arg0.getComponent().getSize();
        final SeamCarver seamCarver = panel.getSeamCarver();
        if (seamCarver != null) {
            while (size.getHeight() < seamCarver.getHeight()) {
                seamCarver.removeHorizontalSeam(seamCarver.findHorizontalSeam());
                LOGGER.debug("Horizontal resize {}x{}", seamCarver.getWidth(), seamCarver.getHeight());
            }
            while (size.getWidth() < seamCarver.getWidth()) {
                seamCarver.removeVerticalSeam(seamCarver.findVerticalSeam());
                LOGGER.debug("Vertical resize {}x{}", seamCarver.getWidth(), seamCarver.getHeight());
            }
        }
    }
}
